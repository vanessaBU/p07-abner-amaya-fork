﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public float speed;
    private new Transform transform;

    // Use this for initialization
    void Start () {
        transform = GetComponent<Transform>();
	}

    // Update is called once per frame
    void Update()
    {
        //Bullet moves forward at speed.
        transform.position += transform.forward * (Time.deltaTime * speed);
    }
}
