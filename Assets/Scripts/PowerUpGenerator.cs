﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PowerUpGenerator : MonoBehaviour
{
    private System.Random rand; // random number generator
    private int framesUntilNext; // # of frames until next power-up

    public GameObject healthPrefab;
    public GameObject tripleShotPrefab;
    public PlayerController player;
    private GameObject powerUp;
    public float speed = 3; // speed of power-up object
    public float powerUpScale = 2;
    public float minSecsBetween;
    public float maxSecsBetween;
    public float xBound;
    public float zSpawnPos;
    // Use this for initialization
    void Start()
    {
        framesUntilNext = generateRandom();
        powerUp = null;
    }

    //Using FixedUpdate becase Update shouldn't be tied to game logic unnecessarily, like it is in the "else"
    //(because Update happens at the games frames-per-second, which can vary)
    void FixedUpdate()
    {
        if (player.tripleShot)
        {
            //Don't spawn more powerups if you're in tripleshot
            return;
        }
        if (powerUp != null)
        {
            powerUp.transform.position += new Vector3(0, 0, -speed*.05f);
        }
        else
        {
            if (framesUntilNext == 0)
            {
                // create power-up object
                Debug.Log("Adding power-up object to scene");
                Vector3 position = new Vector3(Random.Range(-xBound, xBound), 0, zSpawnPos);
                Quaternion rotation = Quaternion.identity;
                //1/3 chance of generating a tripleshot prefab
                GameObject toInstantiate = (Random.Range(0, 3) == 2) ? tripleShotPrefab : healthPrefab;
                powerUp = Instantiate(toInstantiate, position, rotation) as GameObject;

                powerUp.transform.localScale = new Vector3(powerUpScale, powerUpScale, powerUpScale);
                framesUntilNext = generateRandom(); // set # of frames until next power-up
            }
            framesUntilNext--;
        }
    }

    private int generateRandom()
    {
        return Random.Range((int)minSecsBetween*60, (int)maxSecsBetween*60 + 1); // max range non-inclusive
    }
}
