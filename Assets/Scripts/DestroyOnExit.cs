﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnExit : MonoBehaviour {

	void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "PlayerBullet" || collider.gameObject.tag == "EnemyBullet")
        {
            collider.gameObject.SetActive(false);
        }
        if (collider.gameObject.tag == "Flowerpot_pink" || collider.gameObject.tag == "Flowerpot_yellow")
        {
            Destroy(collider.gameObject);
        }

    }
}
