﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossShip : MonoBehaviour {
    public float bulletSpawnOffset = 0.5f;

    // status
    public int health;
    public Text healthText;
    public float minBulletScale;
    public float maxBulletScale;
    public int minTimesToShoot;
    public int maxTimesToShoot;
    public float minSpinSpeed;
    public float maxSpinSpeed;
    public float minTimeBetweenMoves;//in seconds
    public float maxTimeBetweenMoves;
    public float minTimeBetweenAttacks;//in seconds
    public float maxTimeBetweenAttacks;
    public float minBulletSpeed;
    public float maxBulletSpeed;
    public float xBound;
    public float minMoveTime;//length of a move in seconds
    public float maxMoveTime;
    public int minBulletsAround;
    public int maxBulletsARound;
    public float minTimeBetweenShots;
    public float maxTimeBetweenShots;

    private float timeSinceLastAttack = 0;
    private float timeSinceLastMove = 0;
    private float timeUntilNextAttack = 0;
    private float timeUntilNextMove = 0;
    private bool justFinishedMove;
    private bool justFinishedAttack;
    
    private bool isShooting;
    private bool isMoving;

    private AudioSource soundSource;
    public AudioClip laserSound;
    public AudioClip damageSound;
    public GameObject explosionPrefab;


    //public GameObject enemyBulletPrefab;
    private ObjectPooler pool;
    bool done = false;

    private void Awake()
    {
        soundSource = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start () {
        pool = FindObjectOfType<ObjectPooler>();
        healthText = GameObject.Find("EnemyHealth").GetComponent<Text>();
        healthText.text = "Enemy Health: " + health;
        isMoving = false;
        isShooting = false;
    }
	
	// Update is called once per frame
	void Update ()
    { 
        if (justFinishedAttack)
        {
            timeUntilNextAttack = Random.Range(minTimeBetweenAttacks, maxTimeBetweenAttacks);
            justFinishedAttack = false;
        }
        if (justFinishedMove)
        {
            timeUntilNextMove = Random.Range(minTimeBetweenMoves, maxTimeBetweenMoves);
            justFinishedMove = false;
        }
        if (!isMoving)
        {
            if (timeUntilNextMove <= 0)
            {
                float moveDest = Random.Range(-xBound, xBound);
                StartCoroutine(moveToPos(new Vector3(moveDest, transform.position.y, transform.position.z), Random.Range(minMoveTime, maxMoveTime)));
            }
            else
            {
                timeUntilNextMove -= Time.deltaTime;
            }
        }
        if (!isShooting)
        {
            if (timeUntilNextAttack <= 0)
            {
                int numAround = Random.Range(minBulletsAround, maxBulletsARound + 1);
                int numTimesToShoot = Random.Range(minTimesToShoot, maxTimesToShoot);
                bool spin = (Random.Range(0, 2) == 0) ? true : false;
                float spinSpeed = Random.Range(minSpinSpeed, maxSpinSpeed);
                float timeBetweenShots = Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
                float bulletScale = Random.Range(minBulletScale, maxBulletScale);
                float bulletSpeed = Random.Range(minBulletSpeed, maxBulletSpeed);
                StartCoroutine(evenSpread(numAround, numTimesToShoot, spin, spinSpeed, timeBetweenShots, bulletScale, bulletSpeed));
            }
            else
            {
                timeUntilNextAttack -= Time.deltaTime;
            }
        }
        healthText.text = "Enemy Health: " + health;
    }

    //Move from current position to destPos over seconds
    //Based on code from here:
    //http://answers.unity3d.com/questions/192438/coroutines-and-lerp-how-to-make-them-friends.html
    IEnumerator moveToPos(Vector3 destPos, float seconds)
    {
        isMoving = true;
        float timeElapsed = 0;
        Vector3 startPos = transform.position;
        while (timeElapsed < seconds)
        {
            transform.position = Vector3.Lerp(startPos, destPos, timeElapsed / seconds);
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        isMoving = false;
        justFinishedMove = true;
    }

    IEnumerator evenSpread(int numAround, int numTimesToShoot, bool spin, float spinSpeed, float timeBetweenShots, float bulletScale, float bulletSpeed)
    {
        //SetActive numAround bullets evenly spaced bulletSpawnOffset away from ship (setting their rotations facing outward)
        //if spin, rotate ship by spinSpeed
        isShooting = true;
        while (true)
        {
            float angleBetween = 360f / numAround;
            for (int i = 0; i < numAround; i++)
            {
                GameObject bulletObj = pool.GetPooledObject("EnemyBullet");
                var xPos = transform.position.x + bulletSpawnOffset * Mathf.Sin(angleBetween * i * Mathf.Deg2Rad);
                var zPos = transform.position.z + bulletSpawnOffset * Mathf.Cos(angleBetween * i * Mathf.Deg2Rad);

                bulletObj.transform.position = new Vector3(xPos, transform.position.y, zPos);
                bulletObj.transform.rotation = Quaternion.LookRotation(bulletObj.transform.position - transform.position);
                bulletObj.transform.localScale = new Vector3(bulletScale, bulletScale, bulletScale);
                bulletObj.SetActive(true);
                Bullet bullet = bulletObj.GetComponent<Bullet>();
                bullet.speed = bulletSpeed;

                // play laser sound when shooting a bullet
                soundSource.PlayOneShot(laserSound, 0.025F);
            }
            if (numTimesToShoot-- > 0)
            {
                if (spin)
                {
                    //rotate spinSpeed degrees - rotation ends when next shot starts
                    StartCoroutine(RotateOverTime(new Vector3(0, spinSpeed, 0), timeBetweenShots));
                }
                yield return new WaitForSeconds(timeBetweenShots);
            } else
            {
                //set rotation back to original
                transform.rotation = Quaternion.LookRotation(new Vector3(0, 0, 0), new Vector3(0, 0, 0));
                isShooting = false;
                justFinishedAttack = true;
                break;
            }
        }
    }

    //From here:
    //http://answers.unity3d.com/questions/672456/rotate-an-object-a-set-angle-over-time-c.html
    IEnumerator RotateOverTime(Vector3 byAngles, float inTime)
    {
        var fromAngle = transform.rotation;
        var toAngle = Quaternion.Euler(transform.eulerAngles + byAngles);
        for (var t = 0f; t < 1; t += Time.deltaTime / inTime)
        {
            transform.rotation = Quaternion.Lerp(fromAngle, toAngle, t);
            yield return null;
        }
    }

    private void OnTriggerEnter(Collider other) // other is the player's bullet
    {
        if (other.gameObject.tag == "PlayerBullet") // get game object of bullet and check tag
        {
            health--; // reduce player health
            soundSource.PlayOneShot(damageSound, .5F); // play damage sound when hit by player bullet
            other.gameObject.SetActive(false); // return bullet to object pool
            if (health == 0) // game over - player wins
            {
                Vector3 explodePos = transform.position;   
                Destroy(gameObject);
                Instantiate(explosionPrefab, explodePos, Quaternion.identity);
                healthText.text = "Enemy Health: " + health; // final update
            }
        }
    }


}
