﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour {

    public PlayerController player;
    public BossShip boss;
    Animator anim;
    float restartTimer;
    float restartDelay = 5;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    // Use this for initialization
    void Start ()
    {
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (player.health <= 0)
        {
            anim.SetTrigger("GameOver");
            restartTimer += Time.deltaTime;
            // re-load scene
            if (restartTimer >= restartDelay)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        } else if (boss.health <= 0)
        {
            var gameOverText = GameObject.Find("GameOverText").GetComponent<Text>();
            //Spaces because the text looks weird without them.
            gameOverText.text = "Yo u W in !";
            anim.SetTrigger("GameOver");
            restartTimer += Time.deltaTime;
            // re-load scene
            if (restartTimer >= restartDelay)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }
}
