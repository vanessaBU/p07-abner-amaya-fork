﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour {
    public float speed;
    public float xBound;
    public float zBoundTop;
    public float zBoundBottom;
    public int framesBetweenShots = 10;
    //How many units away the bullet spawns from player
    public float bulletZOffset = 0;
    public int bulletSpeed = 15;
    public int healthUp;
    public float tripleShotLength = 10;
    public float tripleShotAngle = 45;

    //When counter is 0 and fire button is held, fire
    private int counter = 0;
    private new Transform transform;
    private ObjectPooler pool;
    private AudioSource soundSource;
    public AudioClip laserSound;
    public AudioClip growSound;
    public float shrinkTime;

    // status
    public int health;
    public Text healthText;

    public bool tripleShot = false;
    private float secsIntoTripleShot = 0;

    private void Awake()
    {
        soundSource = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start () {
        //get the gameobject's transform
        transform = GetComponent<Transform>();
        pool = FindObjectOfType<ObjectPooler>();
        healthText = GameObject.Find("PlayerHealth").GetComponent<Text>();
        healthText.text = "Player Health: " + health;
    }
	
	// Update is called once per frame
	void Update () {
        //Get the current vertical and horizontal inputs
        var xMov = CrossPlatformInputManager.GetAxis("Horizontal");
        var zMov = CrossPlatformInputManager.GetAxis("Vertical");
        if (xMov != 0)
        {
            Debug.Log("xMov=" + xMov);
        }
        if (zMov != 0)
        {
            //Debug.Log("zMov=" + zMov);
        }
        
        //Make sure you can't move outside the screen boundaries.
        if (transform.position.x > xBound && xMov > 0 || transform.position.x < -xBound && xMov < 0) xMov = 0;
        if (transform.position.z > zBoundTop && zMov > 0 || transform.position.z < zBoundBottom && zMov < 0) zMov = 0;

        //Move the character
        //(Deltatime gets the seconds since the last time Update was called)
        transform.localPosition += new Vector3(Time.deltaTime * xMov * speed, 0, Time.deltaTime * speed * zMov);
        healthText.text = "Player Health: " + health;
    }

    //FixedUpdate is called 60 times per second
    void FixedUpdate()
    {
        if (tripleShot)
        {
            secsIntoTripleShot += Time.deltaTime;
        }

        if (secsIntoTripleShot >= tripleShotLength)
        {
            tripleShot = false;
            secsIntoTripleShot = 0;
        }

        if (CrossPlatformInputManager.GetButton("Shoot"))
        {
            if (counter == 0)
            {
                if (!tripleShot)
                {
                    Debug.Log("Getting bullet");
                    GameObject bullet = pool.GetPooledObject("PlayerBullet");
                    Bullet bb = bullet.GetComponent<Bullet>();
                    bullet.transform.position = transform.position + new Vector3(0, 0, bulletZOffset);
                    bullet.transform.rotation = Quaternion.identity;
                    bullet.SetActive(true);
                    bb.speed = bulletSpeed;
                }
                else
                {
                    GameObject bulletMid = pool.GetPooledObject("PlayerBullet");
                    Bullet bbM = bulletMid.GetComponent<Bullet>();
                    bulletMid.transform.rotation = Quaternion.identity;
                    bulletMid.transform.position = transform.position + new Vector3(0, 0, bulletZOffset);
                    bulletMid.SetActive(true);
                    bbM.speed = bulletSpeed;

                    GameObject bulletLeft = pool.GetPooledObject("PlayerBullet");
                    Bullet bbL = bulletLeft.GetComponent<Bullet>();
                    bulletLeft.transform.position = transform.position + new Vector3(0, 0, bulletZOffset);
                    bulletLeft.transform.rotation = Quaternion.Euler(0, 360f - tripleShotAngle, 0);
                    bulletLeft.SetActive(true);
                    bbL.speed = bulletSpeed;

                    GameObject bulletRight = pool.GetPooledObject("PlayerBullet");
                    Bullet bbR = bulletRight.GetComponent<Bullet>();                                        
                    bulletRight.transform.position = transform.position + new Vector3(0, 0, bulletZOffset);                    
                    bulletRight.transform.rotation = Quaternion.Euler(0, tripleShotAngle, 0);
                    bulletRight.SetActive(true);                    
                    bbR.speed = bulletSpeed;
                }

                // play laser sound when shooting a bullet
                soundSource.PlayOneShot(laserSound, 1F);
            }
            counter = (counter + 1) % framesBetweenShots;
        } else
        {
            counter = 0;
        }
    }
    
    private void OnTriggerEnter(Collider other) // other is the enemy's bullet
    {
        if (other.gameObject.tag == "EnemyBullet") // get game object of bullet and check tag
        {
            health--; // reduce player health
            other.gameObject.SetActive(false); // return bullet to object pool
            if (health == 0) // game over - player dies
            {
                StartCoroutine(ShrinkAndDestroy());
                
            }
        }
        else if (other.gameObject.tag == "Flowerpot_pink")
        {
            health += healthUp;
            soundSource.PlayOneShot(growSound, 1F); // play sound
            Destroy(other.gameObject);
        }
        else if (other.gameObject.tag == "Flowerpot_yellow")
        {
            tripleShot = true;
            Destroy(other.gameObject);
        }
    }

    IEnumerator ShrinkAndDestroy()
    {
        float timeElapsed = 0;
        float startScale = transform.localScale.x;
        while (timeElapsed < shrinkTime)
        {          
            var scaleVal = Mathf.Lerp(startScale, 0, timeElapsed / shrinkTime);
            transform.localScale = new Vector3(scaleVal, scaleVal, scaleVal);
            timeElapsed += Time.deltaTime;
            yield return null;
        }
        Destroy(gameObject);
        healthText.text = "Player Health: " + health; // final update
    }

}
